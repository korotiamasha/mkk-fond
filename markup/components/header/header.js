$('.header__burger').on('click', function () {
    $('.mob-nav').addClass('show');
    $('body').addClass('modal-open');
});

$('.has-child>a').on('click', function (e) {
    console.log('5');
    e.preventDefault();
    $('.mob-nav').addClass('submenu-open');
});

$('.mob-nav__back').on('click', function () {
    if ($('.mob-nav').hasClass('submenu-open')) {
        $('.mob-nav').removeClass('submenu-open');
    } else {
        $('.mob-nav').removeClass('show');
        $('body').removeClass('modal-open');
    }
});
