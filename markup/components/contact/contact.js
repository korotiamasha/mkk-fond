if ($('.map').length > 0) {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                center: [55.029358, 82.927329],
                zoom: 17,
                controls: [],
            }, {}),


            MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="popover top balloon-map">' +
                '<div class="close"></div>' +
                '<div class="arrow"></div>' +
                '<div class="popover-inner">' +
                '$[[options.contentLayout observeSize minWidth=235 maxWidth=235 maxHeight=350]]' +
                '</div>' +
                '</div>', {

                    build: function () {
                        this.constructor.superclass.build.call(this);

                        this._$element = $('.popover', this.getParentElement());

                        this.applyElementOffset();

                        this._$element.find('.close')
                            .on('click', $.proxy(this.onCloseClick, this));
                    },

                    clear: function () {
                        this._$element.find('.close')
                            .off('click');

                        this.constructor.superclass.clear.call(this);
                    },


                    onSublayoutSizeChange: function () {
                        MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                        if(!this._isElement(this._$element)) {
                            return;
                        }

                        this.applyElementOffset();

                        this.events.fire('shapechange');
                    },

                    applyElementOffset: function () {
                        this._$element.css({
                            left: -(this._$element[0].offsetWidth / 2),
                            top: -(this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight)
                        });
                    },

                    onCloseClick: function (e) {
                        e.preventDefault();

                        this.events.fire('userclose');
                    },

                    getShape: function () {
                        if(!this._isElement(this._$element)) {
                            return MyBalloonLayout.superclass.getShape.call(this);
                        }

                        var position = this._$element.position();

                        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                            [position.left, position.top], [
                                position.left + this._$element[0].offsetWidth,
                                position.top + this._$element[0].offsetHeight + this._$element.find('.arrow')[0].offsetHeight
                            ]
                        ]));
                    },

                    _isElement: function (element) {
                        return element && element[0] && element.find('.arrow')[0];
                    }
                }),


            MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div class="balloon-header">$[properties.balloonHeader]</div>' +
                '<div class="balloon-body">$[properties.balloonContent]</div>'
            ),


            myPlacemark = window.myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                balloonHeader:  '<h4>МКК ФОНД \n' +
                    'МИКРОФИНАНСИРОВАНИЯ НСО</h4>',

                balloonContent:  '<p>ул. Депутатская, дом 48, офис 610</p>  \n' +
                    '<p>Пн-Пт: 09:00 - 18:00</p>',


            }, {
                balloonShadow: false,
                balloonLayout: MyBalloonLayout,
                balloonContentLayout: MyBalloonContentLayout,
                balloonPanelMaxMapArea: 0,
            });

        myMap.geoObjects.add(myPlacemark);
    });
}

$(function () {
    $('#set-balloon-header').click(function () {
        window.myPlacemark.properties.set(
            'balloonHeader',
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            + 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        );
    });
    $('#set-balloon-content').click(function () {
        window.myPlacemark.properties.set(
            'balloonContent',
            'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
            + 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        );
    });
});
