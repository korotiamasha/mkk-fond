var partnersSlider = new Swiper('.main-partners__slider', {
    speed: 400,
    spaceBetween: 24,
    slidesPerView: 2,
    loop: true,
    navigation: {
        nextEl: '.main-partners__next',
        prevEl: '.main-partners__prev',
    },
    breakpoints: {
        576: {
            slidesPerView: 3,
        },
        768: {
            slidesPerView: 4,
        },
        992: {
            slidesPerView: 5,
        },
        1200: {
            slidesPerView: 6,
        },
    },
});

var contentSlider = new Swiper('.slider-content-swiper', {
    speed: 400,
    spaceBetween: 20,
    slidesPerView: 2,
    loop: true,
    navigation: {
        nextEl: '.slider-content__next',
        prevEl: '.slider-content__prev',
    },
    breakpoints: {
        768: {
            slidesPerView: 3,
        },
        992: {
            slidesPerView: 3,
        },
        1200: {
            slidesPerView: 4,
        },
    },
});

var mainBannerSlider = new Swiper('.main-banner__slider', {
    speed: 400,
    spaceBetween: 0,
    slidesPerView: 1,
    loop: true,
    pagination: {
        el: '.main-banner__pagination',
        type: 'bullets',
        clickable: true,
    },
});

var historySlider = new Swiper('.main-history__slider', {
    speed: 400,
    spaceBetween: 30,
    slidesPerView: "auto",
    loop: false,
    navigation: {
        nextEl: '.main-history__next',
        prevEl: '.main-history__prev',
    },
    breakpoints: {
        1200: {
            loop: true,
            slidesPerView: 2,
        },
    },
});


$('.date-input').datepicker({
    autoClose: true,
});

$('.selectTypeTerm').select2({
    minimumResultsForSearch: -1,
    placeholder: "Выбрать...",
});

$('.select2').select2({
    minimumResultsForSearch: -1,
    placeholder: "",
});

$('#rangeSum').slider({
    /*formatter: function(value) {
        return  value;
    }*/
});
$('#rangeTerm').slider({
    /*formatter: function(value) {
        return  value;
    }*/
});

$('.calc-form__graphic').on('click', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.graph-table').removeClass('show');
    } else {
        $(this).addClass('active');
        $('.graph-table').addClass('show');
    }
});

var phone = document.getElementsByClassName('input-phone');
Array.prototype.forEach.call(phone, function(element) {
    var phoneMask = new IMask(element, {
        mask: '+{7}(000)000-00-00',
        lazy: false,
    });
});


lightbox.option({
    'alwaysShowNavOnTouchDevices': true,
    'wrapAround': true,
    'showImageNumberLabel': false,
    "maxWidth": "980px",
});

$('.lk-table1__edit').on('click', function () {
    if (($(this).attr('data-edit')) == 'false') {
        $('.lk-table1').addClass('edit');
        $(this).attr('data-edit', 'true');
        $(this).html('<span class="icon-edit"></span>Отменить');
    } else {
        $('.lk-table1').removeClass('edit');
        $(this).attr('data-edit', 'false');
        $(this).html('<span class="icon-edit"></span>Редактировать');
    }
});
